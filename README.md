# Cornershop Frontend Test v1.5.0

## Getting started

First and foremost, make sure you have `node` and `npm` (or `yarn`) installed on your machine, then run:

Clone or download the repository

To clone by terminal

```bash
$ git clone https://github.com/Jadace96/cornershop-frontend-test.git
```

For `yarn` users:

```bash
$ yarn
$ yarn start
```

For run project test:

Run all test

```bash
$ yarn test
```

Watching test changes

```bash
$ yarn test:dev
```

## API endpoints / examples

Since the backend API runs locally on a different port (`3001`) than the `react-scripts` dev server (`3000`), we have setup a proxy so you don't have to do anything special to consume the API (fetching data from `/api/v1/counter` will do).

> The following endpoints are expecting a `Content-Type: application/json` header.

#### **GET** `/api/v1/counter`.

_Fetch a list of counters._

```javascript
/* Response */
[];
```

#### **POST** `/api/v1/counter`.

_Adds a counter._

```javascript
/* Body */
{ title: "bob" }

/* Response */
{ id: "asdf", title: "bob", count: 0 }
```

#### **POST** `/api/v1/counter/inc`

_Increments the value of a counter._

```javascript
/* Body */
{ id: "asdf" }

/* Response */
{ id: "asdf", title: "bob", count: 1 }
```

#### **POST** `/api/v1/counter/dec`

_Decrements the value of a counter._

```javascript
/* Body */
{ id: "asdf" }

/* Response */
{ id: "asdf", title: "bob", count: 0 }
```

#### **DELETE** `/api/v1/counter`

_Deletes a counter._

```javascript
/* Body */
{
  id: 'qwer';
}

/* Response */
('qwer'); // The id of the deleted counter
```

---
