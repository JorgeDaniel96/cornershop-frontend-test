// vendors
import { useEffect } from 'react';

export const csUseOnClickOutside = (elementRef, callback) => {
  useEffect(() => {
    /**
     * Trigger callback cation when clicked outside
     */
    function onClickOutside({ target }) {
      if (elementRef.current && !elementRef.current.contains(target)) {
        callback();
      }
    }

    // Bind the event listener
    document.addEventListener('mousedown', onClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', onClickOutside);
    };
  }, [elementRef]);
};
