// vendors
import { useIntl, FormattedMessage } from 'react-intl';

export const useI18n = () => {
  const { locale = 'en', messages } = useIntl();

  const getMessage = (messageId) => {
    return messages[messageId];
  };

  const getFormattedMessage = (messageId, messageValues) => {
    return <FormattedMessage id={messageId} values={messageValues} />;
  };

  return { locale, getMessage, getFormattedMessage };
};
