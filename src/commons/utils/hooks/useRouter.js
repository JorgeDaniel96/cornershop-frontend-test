// vendors
import { useMemo } from 'react';
import queryString from 'query-string';
import {
  useParams,
  useHistory,
  useLocation,
  useRouteMatch,
} from 'react-router-dom';

// Hook
export function useRouter() {
  const params = useParams();
  const history = useHistory();
  const match = useRouteMatch();
  const location = useLocation();

  // Return our custom router object
  // Memoize so that a new object is only returned if something changes
  return useMemo(() => {
    return {
      // For convenience add goTo(), replace(), pathname at top level
      goTo: history.push,
      goBack: history.goBack,
      replace: history.replace,
      pathname: location.pathname,
      state: location.state,
      deleteState: (name) => {
        const state = { ...location.state };
        delete state[name];
        history.push({
          pathname: location.pathname,
          state,
        });
      },
      setState: (name, state) =>
        history.push({
          pathname: location.pathname,
          state: {
            [name]: state,
          },
        }),
      cleanState: () =>
        history.push({
          pathname: location.pathname,
          state: {},
        }),
      // Merge params and parsed query string into single "query" object
      // so that they can be used interchangeably.
      // Example: /:topic?sort=popular -> { topic: "react", sort: "popular" }
      query: {
        ...queryString.parse(location.search), // Convert string to object
        ...params,
      },
      // Include match, location, history objects so we have
      // access to extra React Router functionality if needed.
      match,
      location,
      history,
    };
  }, [params, match, location, history]);
}
