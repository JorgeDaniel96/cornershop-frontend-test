// vendors
import queryString from 'query-string';

// commons
import { ENDPOINTS } from 'commons';

// constants
const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const appendQueryStringsToUrl = (endpoint, queryStrings) => {
  const queries = queryString.stringify(queryStrings, {
    sort: false,
    skipNull: true,
    skipEmptyString: true,
  });

  const urlWithQueryStrings = `${endpoint}?${queries}`;

  return urlWithQueryStrings;
};

// Build the URl with params and query strings
export const buildUrl = ({
  endpointKey,
  urlParams = {},
  queryStrings = {},
}) => {
  let endpoint = ENDPOINTS[endpointKey]?.url;

  if (Object.values(urlParams).length > 0) {
    endpoint = `${endpoint}/${Object.values(urlParams).join('/')}`;
  }

  if (Object.keys(queryStrings)?.length > 0) {
    const url = appendQueryStringsToUrl(endpoint, queryStrings);

    return url;
  }

  return endpoint;
};

// To retrieve data
export const GET = async ({ urlParams, endpointKey, queryStrings = {} }) => {
  const URL = buildUrl({ urlParams, endpointKey, queryStrings });
  const response = await fetch(URL, {
    method: 'GET',
    headers,
  });
  const dataJson = await response.json();

  return dataJson;
};

// To sends data and creates a new resource
export const POST = async ({
  body,
  urlParams,
  endpointKey,
  queryStrings = {},
}) => {
  const URL = buildUrl({ urlParams, endpointKey, queryStrings });
  const response = await fetch(URL, {
    method: 'POST',
    body: JSON.stringify(body),
    headers,
  });
  const dataJson = await response.json();

  return dataJson;
};

// To update an existing resource
export const PUT = async ({
  body,
  urlParams,
  endpointKey,
  queryStrings = {},
}) => {
  const URL = buildUrl({ urlParams, endpointKey, queryStrings });
  const response = await fetch(URL, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers,
  });
  const dataJson = await response.json();

  return dataJson;
};

// To update an existing resource, the request body only needs to contain the specific changes
export const PATCH = async ({
  body,
  urlParams,
  endpointKey,
  queryStrings = {},
}) => {
  const URL = buildUrl({ urlParams, endpointKey, queryStrings });
  const response = await fetch(URL, {
    method: 'PATCH',
    body: JSON.stringify(body),
    headers,
  });
  const dataJson = await response.json();

  return dataJson;
};

// To delete a resource.
export const DELETE = async ({
  body,
  urlParams,
  endpointKey,
  queryStrings = {},
}) => {
  const URL = buildUrl({ urlParams, endpointKey, queryStrings });
  const response = await fetch(URL, {
    method: 'DELETE',
    body: JSON.stringify(body),
    headers,
  });
  const dataJson = await response.json();

  return dataJson;
};
