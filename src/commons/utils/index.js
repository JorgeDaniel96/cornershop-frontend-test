// vendors
import { sumBy } from 'lodash';

export * from './api';
export * from './hooks';
export * from './testing';
export * from './react-query';

export const csFilterDataByKey = (
  value = '',
  dataToFilter = [],
  keyToFilter = 'title'
) => {
  if (value !== '') {
    const pattern = new RegExp(`${value}`, 'i');
    const filteredData = dataToFilter?.filter(
      (item) => item[keyToFilter] && pattern.test(item[keyToFilter])
    );

    return filteredData;
  }

  return dataToFilter;
};

export const csGetTotalTimes = (counters = []) => {
  const totalTimes = sumBy(counters, 'count');

  return totalTimes;
};

export const csCopyToClipboard = async (textToCopy = '') => {
  await navigator.clipboard.writeText(textToCopy);
};
