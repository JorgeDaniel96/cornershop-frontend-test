// vendors
import { render } from '@testing-library/react';

// providers
import { CsTestingProviders } from './cs-testing-provider';

const defaultOptions = {
  theme: {},
  locale: 'en',
  messages: {},
};

export const CsTestingCustomRender = (
  component,
  providerOptions = defaultOptions
) =>
  render(component, {
    wrapper: CsTestingProviders(providerOptions),
  });
