// vendors
import { IntlProvider } from 'react-intl';
import { ThemeProvider } from 'styled-components';
import { HelmetProvider } from 'react-helmet-async';
import { queryClient, ReactQueryProvider } from '../react-query';

export const CsTestingProviders = ({
  theme = {},
  locale = 'en',
  messages = {},
  reactQueryClient = queryClient,
}) => ({ children }) => (
  <HelmetProvider>
    <ReactQueryProvider client={reactQueryClient}>
      <ThemeProvider theme={theme}>
        <IntlProvider locale={locale} messages={messages}>
          {children}
        </IntlProvider>
      </ThemeProvider>
    </ReactQueryProvider>
  </HelmetProvider>
);
