// vendors
import {
  useQuery,
  QueryClient,
  useMutation,
  QueryClientProvider,
} from 'react-query';

// constants
const defaultConfig = {
  retry: false,
  enabled: false,
  refetchOnMount: false,
  refetchOnReconnect: false,
  refetchOnWindowFocus: false,
};

// https://react-query.tanstack.com/reference/QueryClientProvider#_top
const queryClient = new QueryClient();
export { queryClient, QueryClientProvider as ReactQueryProvider };

// https://react-query.tanstack.com/reference/QueryClient#queryclientgetquerydata
export function csGetQueryData(queryKey) {
  const queryData = queryClient.getQueryData(queryKey);

  return queryData;
}

// https://react-query.tanstack.com/reference/QueryClient#queryclientsetquerydata
export function csSetQueryData(queryKey, updater) {
  queryClient.setQueryData(queryKey, updater);
}

// https://react-query.tanstack.com/guides/queries#_top
export function csUseQuery({ key, service, config = defaultConfig }) {
  return useQuery(key, service, config);
}

// https://react-query.tanstack.com/guides/mutations
export function csUseMutation({ service, config = {} }) {
  return useMutation(service, {
    ...config,
  });
}
