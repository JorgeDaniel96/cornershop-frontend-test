// commons
import { QUERY_KEYS, csSetQueryData } from 'commons';

export const csUpdateCounterQueryOnCountChange = (counterUpdated) => {
  csSetQueryData(QUERY_KEYS.COUNTERS, (prevCounters) => {
    const updatedCounters = prevCounters?.map((counter) =>
      counterUpdated.id === counter.id ? counterUpdated : counter
    );

    return updatedCounters;
  });
};

export const csUpdateCounterQueryOnDelete = (counterDeletedId) => {
  let updatedCounters = [];
  csSetQueryData(QUERY_KEYS.COUNTERS, (prevCounters = []) => {
    if (prevCounters.length > 1) {
      updatedCounters = prevCounters.filter(
        (counter) => counter.id !== counterDeletedId
      );
    }

    return updatedCounters;
  });
};
