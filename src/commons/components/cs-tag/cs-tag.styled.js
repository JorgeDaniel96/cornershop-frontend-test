// vendors
import styled from 'styled-components';

export const Container = styled.div.attrs(() => ({
  className: 'cs-tag-container',
}))`
  margin: 8px;
  height: 40px;
  cursor: pointer;
  padding: 8px 15px;
  position: relative;
  text-align: center;
  border-radius: 99px;
  white-space: nowrap;
  display: inline-block;
  min-width: fit-content;
  text-overflow: ellipsis;
  background-color: #ececec;
  border: 1px solid ${({ theme }) => theme.colors.silver};
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.1);
  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }
  :hover {
    .cs-tag-close-icon {
      display: initial;
    }
  }
`;
Container.displayName = 'cs-tag-container';

export const CloseIcon = styled.img.attrs(() => ({
  className: 'cs-tag-close-icon',
}))`
  display: initial;

  ${({ theme }) => theme.device.tabletAndDesktop} {
    right: 10px;
    width: 30px;
    height: 30px;
    display: none;
    cursor: pointer;
    position: absolute;
    * {
      fill: #acacac;
    }
  }
`;
CloseIcon.displayName = 'cs-tag-close-icon';
