// vendors
import React from 'react';

// commons
import { CsSVG } from 'commons';

// styles
import { Container, CloseIcon } from './cs-tag.styled';

export function CsTag({
  children,
  onClickIcon,
  iconName = 'CloseImg',
  ...rest
}) {
  return (
    <Container {...rest}>
      {children}
      {onClickIcon ? (
        <CloseIcon as={CsSVG[iconName]} onClick={() => onClickIcon(children)} />
      ) : (
        <></>
      )}
    </Container>
  );
}
