import React from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';
import Transition from 'react-transition-group/Transition';

import './cs-modal.css';
import { CsText } from '../cs-text';

/**
 * Used for controlling an Modal
 */
export const csUseModal = () => {
  const [showModal, setShowModal] = React.useState(false);

  const toggleModal = (show) => {
    setShowModal(typeof show === 'boolean' ? show : !showModal);
  };

  return [showModal, toggleModal];
};

const TRANSITION_TIMEOUT = 295; // ms

const Modal = ({
  children,
  className = '',
  showModal = false,
  onClose,
  onOpen,
  ...rest
}) => {
  const modalContentRef = React.useRef();

  const handleEntered = () => {
    modalContentRef.current.focus();

    typeof onOpen === 'function' && onOpen();
  };

  const handleExited = () => {
    typeof onClose === 'function' && onClose();
  };

  return ReactDOM.createPortal(
    <Transition
      in={showModal}
      timeout={TRANSITION_TIMEOUT}
      mountOnEnter
      unmountOnExit
      onEntered={handleEntered}
      onExited={handleExited}
    >
      {(status) => (
        <div
          className={classnames('cs-modal', className)}
          role='dialog'
          aria-labelledby='modal-title'
          aria-modal='true'
          tabIndex='-1'
          {...rest}
        >
          <div
            className={`cs-modal__backdrop cs-fade-transition cs-fade-${status}`}
          />
          <div
            ref={modalContentRef}
            className={`cs-modal__content cs-modal-slide-transition cs-modal-slide-${status}`}
            tabIndex='-1'
            role='document'
          >
            {children}
          </div>
        </div>
      )}
    </Transition>,
    document.getElementById('modal-outlet')
  );
};

const ModalHeader = ({ children, className, ...rest }) => {
  const classes = classnames('cs-modal__header', className);

  return (
    <header className={classes} {...rest}>
      {children}
    </header>
  );
};

const ModalBody = ({ children, className, ...rest }) => {
  const classes = classnames('cs-modal__body', className);

  return (
    <section className={classes} {...rest}>
      {children}
    </section>
  );
};

const ModalTitle = ({ children, className, ...rest }) => {
  const classes = classnames('cs-modal__title', className);
  return (
    <CsText
      type='big'
      as='h2'
      className={classes}
      id='cs-modal-title'
      {...rest}
    >
      {children}
    </CsText>
  );
};

Modal.Body = ModalBody;
Modal.Header = ModalHeader;
Modal.Title = ModalTitle;

export default Modal;
