// vendors
import styled, { css } from 'styled-components';

const disabledStyles = css`
  opacity: 0.5;
  pointer-events: none;
`;

export const Container = styled.div.attrs(() => ({
  className: 'cs-sercher-container',
}))`
  display: flex;
  max-width: 379px;
  position: relative;
  margin: 0 auto 26px;
  align-items: center;

  .cs-search-icon {
    left: 22.02px;
    position: absolute;
    & * {
      fill: ${({ theme }) => theme.colors.grey};
    }
  }

  .cs-input {
    font-size: 16px;
    padding-left: 56px;
    color: ${({ theme }) => theme.colors.grey};

    :focus {
      color: initial;
    }
  }

  .cs-searcher-button {
    font-size: 17px;
    font-weight: 600;
    margin-left: 12px;
  }

  ${({ disabled = false }) => (disabled ? disabledStyles : ``)}
`;
Container.displayName = 'CsSercherContainer';
