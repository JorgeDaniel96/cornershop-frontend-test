// vendors
import React, { useState } from 'react';

// components
import { CsButton, CsInput } from '..';

// icons
import { CsSearchIcon } from '../../icons';

// styles
import { Container } from './cs-searcher.styled';

const BASE_TEST_ID = 'CsSearcher';

export function CsSearcher({
  inputConfig,
  buttonConfig,
  disabled = false,
  onFocusSercher = () => {},
}) {
  const [isSearchInputFocused, setIsSearchInputFocused] = useState(false);

  const onFocusChange = ({ type }) => {
    const isFocuses = type === 'focus';
    onFocusSercher(isFocuses);
    setIsSearchInputFocused(isFocuses);
  };

  return (
    <Container disabled={disabled} data-testid={`${BASE_TEST_ID}Container`}>
      <CsSearchIcon data-testid={`${BASE_TEST_ID}Icon`} />
      <CsInput
        onBlur={onFocusChange}
        onFocus={onFocusChange}
        value={inputConfig.value}
        onChange={inputConfig.onChange}
        placeholder={inputConfig.placeholder}
      />
      {isSearchInputFocused || inputConfig.value?.length > 0 ? (
        <CsButton
          size='big'
          color='white'
          className='cs-searcher-button'
          data-testid={`${BASE_TEST_ID}Button`}
          onClick={buttonConfig.onClick}
          {...buttonConfig}
        >
          {buttonConfig.content}
        </CsButton>
      ) : (
        <></>
      )}
    </Container>
  );
}
