// vendors
import { useState } from 'react';

export function csUseSercher(initialValue = '') {
  const [searchInputValue, setSearchInputValue] = useState(initialValue);

  return {
    searchInputValue,
    setSearchInputValue,
    onSearchInputChange: setSearchInputValue,
  };
}
