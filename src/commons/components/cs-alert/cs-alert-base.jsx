// vendors
import React from 'react';

// commons
import { CsButton } from 'commons';

// components
import { CsAlert } from '.';

export function CsAlertBase({
  title,
  message,
  showAlert,
  actionButtonContent,
  secondaryActionButtonContent,
  primaryButtonClick = () => {},
  secondaryButtonClick = () => {},
}) {
  return (
    <CsAlert showAlert={showAlert}>
      <CsAlert.Title>{title}</CsAlert.Title>
      <CsAlert.Message>{message}</CsAlert.Message>
      <CsAlert.Actions>
        <CsButton onClick={primaryButtonClick}>{actionButtonContent}</CsButton>
        {secondaryActionButtonContent && (
          <CsButton
            color='white'
            onClick={secondaryButtonClick}
            className='secondary-action-button-content'
          >
            {secondaryActionButtonContent}
          </CsButton>
        )}
      </CsAlert.Actions>
    </CsAlert>
  );
}
