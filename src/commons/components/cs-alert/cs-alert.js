import React from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';
import Transition from 'react-transition-group/Transition';

import './cs-alert.css';
import { CsText } from '../cs-text';

/**
 * Used for controlling an Alert
 */
export const csUseAlert = (initiaValue = false) => {
  const [showAlert, setShowAlert] = React.useState(initiaValue);

  const toggleAlert = (show) => {
    setShowAlert(typeof show === 'boolean' ? show : !showAlert);
  };

  return [showAlert, toggleAlert];
};

const TRANSITION_TIMEOUT = 195; // ms

export const CsAlert = ({
  children,
  className = '',
  showAlert,
  onClose,
  onOpen,
  ...rest
}) => {
  const alertContentRef = React.useRef();

  const handleEntered = () => {
    alertContentRef.current.focus();

    typeof onOpen === 'function' && onOpen();
  };

  const handleExited = () => {
    typeof onClose === 'function' && onClose();
  };

  return ReactDOM.createPortal(
    <Transition
      in={showAlert}
      timeout={TRANSITION_TIMEOUT}
      mountOnEnter
      unmountOnExit
      onEntered={handleEntered}
      onExited={handleExited}
    >
      {(status) => (
        <div
          className={classnames('cs-alert', className)}
          role='dialog'
          aria-labelledby='alert-title'
          aria-modal='true'
          tabIndex='-1'
          {...rest}
        >
          <div
            className={`cs-alert__backdrop cs-fade-transition cs-fade-${status}`}
          />
          <div
            ref={alertContentRef}
            className={`cs-alert__content cs-alert-slide-transition cs-alert-slide-${status}`}
            tabIndex='-1'
            role='document'
          >
            {children}
          </div>
        </div>
      )}
    </Transition>,
    document.getElementById('alert-outlet')
  );
};

// Allowed Child Components

const AlertActions = ({ children, className, ...rest }) => {
  const classes = classnames('cs-alert__actions', className);

  return (
    <div className={classes} {...rest}>
      {children}
    </div>
  );
};

const AlertMessage = ({ children, className, ...rest }) => {
  const classes = classnames('cs-alert__message', className);

  return (
    <p className={classes} {...rest}>
      {children}
    </p>
  );
};

const AlertTitle = ({ children, className, ...rest }) => {
  const classes = classnames('cs-alert__title alert-title', className);

  return (
    <CsText
      as='h2'
      type='big'
      className={classes}
      {...rest}
      id='cs-alert-title'
    >
      {children}
    </CsText>
  );
};

CsAlert.Actions = AlertActions;
CsAlert.Message = AlertMessage;
CsAlert.Title = AlertTitle;
