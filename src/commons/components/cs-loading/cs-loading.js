// vendors
import React from 'react';

// styles
import './cs-loading.css';
import { Container } from './cs-loading.styled';

export const Loading = () => (
  <div className='cs-loading-spinner' data-testid={`CsLoading`}>
    <div className='cs-loading-spinner__bouncer'></div>
    <div className='cs-loading-spinner__bouncer'></div>
    <div className='cs-loading-spinner__bouncer'></div>
  </div>
);

export function CsLoading(props) {
  return (
    <Container {...props}>
      <Loading />
    </Container>
  );
}
