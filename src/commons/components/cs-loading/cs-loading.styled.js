// vendors
import styled from 'styled-components';

export const Container = styled.div.attrs(() => ({
  className: 'cs-loading-container',
}))`
  top: 0;
  left: 0;
  width: 100%;
  z-index: 10;
  display: flex;
  position: fixed;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.clearWhite};
  height: calc(
    100% - ${({ theme }) => theme.sizes.dashboardFooterButtonHeight}
  );
`;
Container.displayName = 'CsLoadingContainer';
