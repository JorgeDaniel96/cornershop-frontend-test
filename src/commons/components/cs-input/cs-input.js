import React, { useState } from 'react';
import classnames from 'classnames';

import './cs-input.css';

const InputSizeVariant = {
  Regular: 'regular',
  Big: 'big',
};

const InputSizeClasses = {
  [InputSizeVariant.Regular]: '',
  [InputSizeVariant.Big]: 'cs-input--big',
};

const BASE_TEST_ID = 'CsInput';

export const csUseInput = () => {
  const [inpuValue, setInpuValue] = useState('');

  return {
    inpuValue,
    setInpuValue,
    isInputEmpty: inpuValue === '',
  };
};

export const CsInput = ({
  className,
  onChange = () => {},
  onEnterPressed = () => {},
  size = InputSizeVariant.Regular,
  ...rest
}) => {
  const classes = classnames(
    'cs-input',
    InputSizeClasses[InputSizeVariant],
    className
  );

  const onKeyDown = ({ key }) => key === 'Enter' && onEnterPressed();

  return (
    <input
      onKeyDown={onKeyDown}
      onChange={({ target }) => onChange(target.value)}
      data-testid={`${BASE_TEST_ID}`}
      className={classes}
      {...rest}
    />
  );
};
