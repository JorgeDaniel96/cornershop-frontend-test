// vendors
import styled, { css } from 'styled-components';

const counterActiveStyles = css`
  border-radius: 6px;
  background-color: #ffeacc;

  .cs-button {
    background-color: transparent;
  }
`;

export const Container = styled.div.attrs(() => ({
  className: 'cs-counter-container',
}))`
  display: flex;
  align-items: center;
  padding: 5px 0px 5px 16px;
  justify-content: space-between;

  .cs-text {
    width: 100%;
    cursor: pointer;
  }

  ${({ isActive = false }) => (isActive ? counterActiveStyles : <></>)}
`;
Container.displayName = 'CsCounterContainer';

const emptyCounterStyles = ({ colors }) => css`
  .cs-counter-value {
    color: ${colors.grey};
  }

  .cs-button span .cs-decrement-icon * {
    fill: ${({ theme }) => theme.colors.silver};
  }
`;
export const Controls = styled.div.attrs(() => ({
  className: 'cs-counter-controls',
}))`
  display: flex;
  align-items: center;
  .cs-button span svg * {
    fill: ${({ theme }) => theme.colors.appTint};
  }

  ${({ theme, value = 0 }) => (value === 0 ? emptyCounterStyles(theme) : ``)};
`;
Controls.displayName = 'CsCounterControls';

export const Value = styled.span.attrs(() => ({
  className: 'cs-counter-value',
}))`
  min-width: 25px;
  font-size: 20px;
  font-weight: 600;
  line-height: 22px;
  letter-spacing: -0.408px;
  color: ${({ theme }) => theme.colors.darkBlack};
`;
Value.displayName = 'CsCounterValue';
