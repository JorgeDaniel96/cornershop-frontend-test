// vendors
import React from 'react';

// components
import { CsButton } from '../cs-button';

// icons
import { CsDecrementIcon, CsIncrementIcon } from '../../icons';

// styles
import { Container, Controls, Value } from './cs-counter.styled';
import { CsText } from '../cs-text';

// constants
const [DECREASE, INCREASE] = ['decrease', 'increase'];

const csButtonDefaultProps = {
  size: 'big',
  kind: 'flat',
  color: 'white',
};

const BASE_TEST_ID = 'CsCounter';

export function CsCounter({
  counter,
  onClick,
  isActive = false,
  onDecrease = () => {},
  onIncrease = () => {},
}) {
  const counterValue = counter?.count;

  const onCounterChange = (changeType = DECREASE) => {
    if (changeType === DECREASE && counterValue >= 1) {
      const count = counterValue - 1;
      onDecrease({ count, ...counter });
    } else {
      const count = counterValue + 1;
      onIncrease({ count, ...counter });
    }
  };

  return (
    <Container isActive={isActive} data-testid={`${BASE_TEST_ID}Container`}>
      <CsText onClick={() => onClick(counter)}>{counter?.title}</CsText>
      <Controls value={counterValue}>
        <CsButton
          {...csButtonDefaultProps}
          disabled={counterValue < 1}
          onClick={() => onCounterChange()}
        >
          <CsDecrementIcon />
        </CsButton>
        <Value>{counterValue}</Value>
        <CsButton
          {...csButtonDefaultProps}
          onClick={() => onCounterChange(INCREASE)}
        >
          <CsIncrementIcon />
        </CsButton>
      </Controls>
    </Container>
  );
}
