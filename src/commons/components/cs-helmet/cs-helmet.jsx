// vendors
import React from 'react';
import { Helmet } from 'react-helmet-async';

// commons
import { useI18n } from 'commons';

export function CsHelmet({ titleView }) {
  const { getMessage } = useI18n();
  const appName = getMessage('app.name');

  return (
    <Helmet>
      <title>{titleView ? `${appName} - ${titleView}` : appName}</title>
    </Helmet>
  );
}
