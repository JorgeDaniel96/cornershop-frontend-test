// vendors
import styled from 'styled-components';

export const CsVisibility = styled.div.attrs(() => ({
  className: 'cs-visibility',
}))`
  visibility: ${({ visible = false }) => (visible ? 'visible' : 'hidden')};
`;
CsVisibility.displayName = 'CsVisibility';
