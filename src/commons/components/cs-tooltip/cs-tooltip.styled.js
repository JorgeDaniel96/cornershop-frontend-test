// vendors
import styled from 'styled-components';

export const Container = styled.div.attrs(() => ({
  className: 'cs-tooltip-container',
}))`
  display: block;
`;
Container.displayName = 'CsTooltipContainer';

export const ContentWrapper = styled.div.attrs(() => ({
  className: 'cs-tooltip-conten-wrapper',
}))`
  position: relative;
  max-width: 100%;
  min-width: 100%;

  &:after {
    display: ${({ showContent = false }) => (showContent ? 'initial' : 'none')};
    position: absolute;
    left: 20%;
    content: '';
    z-index: 15;
    top: -15.7px;
    width: 0;
    height: 0;
    border: 0 solid transparent;
    border-left-width: 20px;
    border-right-width: 20px;
    border-top: 18px solid #fafafa;
    right: 0;
  }
`;
ContentWrapper.displayName = 'CsTooltipContentWrapper';

export const TooltipConten = styled.div.attrs(() => ({
  className: 'cs-tooltip-content',
}))`
  margin: 0;
  z-index: 10;
  width: auto;
  padding: 13px 18px;
  display: block;
  color: #000000;
  font-size: 14px;
  max-width: calc(100% - 18px);
  position: absolute;
  border-radius: 10px;
  white-space: initial;
  word-break: break-word;
  background-color: #fafafa;
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: 0px 24px 38px rgba(0, 0, 0, 0.14);
  left: ${({ left }) => `${left}px`};
  bottom: ${({ bottom }) => `${bottom}px`};
`;
TooltipConten.displayName = 'CsTooltipContent';
