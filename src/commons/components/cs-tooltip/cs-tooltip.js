// vendors
import { csUseOnClickOutside } from 'commons';
import React, { useEffect, useState, useRef } from 'react';

// styles
import { Container, ContentWrapper, TooltipConten } from './cs-tooltip.styled';

export const CsTooltip = ({
  children,
  contentPopUp,
  show = false,
  tooltipContenParams,
}) => {
  const csTooltipRef = useRef(null);
  const [showTooltip, setShowTooltip] = useState(false);
  csUseOnClickOutside(csTooltipRef, () => setShowTooltip(false));

  useEffect(() => {
    setShowTooltip(show);
  }, [show]);

  return (
    <div ref={csTooltipRef}>
      <Container onClick={() => setShowTooltip(true)}>
        <ContentWrapper showContent={showTooltip}>
          {children({ setShowTooltip })}
        </ContentWrapper>
      </Container>
      {showTooltip && (
        <TooltipConten
          left={
            csTooltipRef.current?.offsetLeft -
            csTooltipRef.current?.offsetWidth -
            18
          }
          bottom={
            csTooltipRef.current?.offsetTop +
            (csTooltipRef.current?.offsetHeight + 15)
          }
          {...tooltipContenParams}
        >
          {contentPopUp({ csTooltipRef, setShowTooltip }) ||
            children({ csTooltipRef, setShowTooltip })}
        </TooltipConten>
      )}
    </div>
  );
};
