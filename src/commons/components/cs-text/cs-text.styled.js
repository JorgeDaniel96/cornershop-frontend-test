// vendors
import styled, { css } from 'styled-components';

const types = {
  big: css`
    font-size: 22px;
    font-weight: bold;
    line-height: 30px;
  `,
  regular: css`
    font-size: 17px;
    line-height: 23px;
  `,
  small: css`
    font-size: 15px;
    line-height: 20px;
  `,
};

export const CsText = styled.span.attrs(({ type = 'regular' }) => ({
  className: `cs-text cs-text-${type}`,
}))`
  padding: 10px 0;
  text-align: left;
  min-width: fit-content;
  font-weight: ${({ weight = 'normal' }) => weight};
  ${({ type = 'regular' }) => types[type]};
`;
CsText.displayName = 'CsText';
