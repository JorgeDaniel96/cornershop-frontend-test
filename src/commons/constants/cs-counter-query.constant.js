// commons
import {
  csSetQueryData,
  counterService,
  csUpdateCounterQueryOnDelete,
  csUpdateCounterQueryOnCountChange,
} from 'commons';

// constants
import { QUERY_KEYS } from '.';

const config = {
  retry: false,
  enabled: false,
  refetchOnMount: false,
  refetchOnReconnect: false,
  refetchOnWindowFocus: false,
};

export const getCountersQuery = {
  key: QUERY_KEYS.COUNTERS,
  service: counterService.getCounters,
  config,
};

export const addCounterQuery = {
  service: counterService.addCounter,
  config: {
    onSuccess: (serviceResponseData) => {
      csSetQueryData(QUERY_KEYS.COUNTERS, (prevData) => [
        ...prevData,
        serviceResponseData,
      ]);
    },
  },
};

export const incrementCounterQuery = ({
  updateQueryOnSuccess = true,
  onUpdateTimeError = () => {},
  onUpdateTimeSuccess = () => {},
}) => {
  const queryConfig = {
    onSuccess: (counterUpdated) => {
      onUpdateTimeSuccess(counterUpdated);
      updateQueryOnSuccess && csUpdateCounterQueryOnCountChange(counterUpdated);
    },
    onError: (_, counter) => {
      const nextCount = counter.count + 1;
      onUpdateTimeError({ ...counter, nextCount, isIncrement: true });
    },
    retry: false,
  };

  return {
    service: counterService.incrementCounter,
    config: queryConfig,
  };
};

export const decrementCounterQuery = ({
  updateQueryOnSuccess = true,
  onUpdateTimeError = () => {},
  onUpdateTimeSuccess = () => {},
}) => {
  const queryConfig = {
    onSuccess: (counterUpdated) => {
      onUpdateTimeSuccess(counterUpdated);
      updateQueryOnSuccess && csUpdateCounterQueryOnCountChange(counterUpdated);
    },
    onError: (_, counter) => {
      const nextCount = counter.count - 1;
      onUpdateTimeError({ ...counter, nextCount, isDecrement: true });
    },
    retry: false,
  };

  return {
    service: counterService.decrementCounter,
    config: queryConfig,
  };
};

export const deleteCounterQuery = ({ updateQueryOnSuccess = true } = {}) => {
  const queryConfig = {
    onSuccess: (counterDeletedId) => {
      updateQueryOnSuccess && csUpdateCounterQueryOnDelete(counterDeletedId);
    },
    retry: false,
  };

  return {
    service: counterService.deleteCounter,
    config: queryConfig,
  };
};
