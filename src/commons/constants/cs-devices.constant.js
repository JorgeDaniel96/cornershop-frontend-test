export const mobileMax = 767;
export const tabletMin = 768;
export const tabletMax = 1023;
export const desktopMin = 1024;


// use like : "@media ${device.mobile} { margin-left: 10px; }"

// MOBILE
export const mobile = `@media (max-width:${mobileMax}px)`;
// MOBILE cross rules
export const mobileAndTablet = `@media (max-width:${tabletMax}px)`;

// TABLET
export const tablet = `@media (min-width:${tabletMin}px) and (max-width:${tabletMax}px)`;
// TABLET cross rules
export const tabletAndDesktop = `@media (min-width:${tabletMin}px)`;

// DESKTOP
export const desktop = `@media (min-width:${desktopMin}px)`;

export const device = {
  mobile,
  tablet,
  desktop,
  mobileAndTablet,
  tabletAndDesktop,
};
