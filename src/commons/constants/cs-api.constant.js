export const BASE_HOST = '/api/v1/counter';

export const ENDPOINTS = {
  GET_COUNTERS: {
    key: 'GET_COUNTERS',
    url: `${BASE_HOST}`,
  },
  POST_COUNTER: {
    key: 'POST_COUNTER',
    url: `${BASE_HOST}`,
  },
  INCREMENT_COUNTER: {
    key: 'INCREMENT_COUNTER',
    url: `${BASE_HOST}/inc`,
  },
  DECREMENT_COUNTER: {
    key: 'DECREMENT_COUNTER',
    url: `${BASE_HOST}/dec`,
  },
  DELETE_COUNTER: {
    key: 'DELETE_COUNTER',
    url: `${BASE_HOST}`,
  },
};
