export const AppColors = {
  appTint: '#ff9500',
  destructiveRed: '#ff3b30',
  white: '#fff',
  darkBlack: '#212121',
  black: '#4a4a4a',
  grey: '#888b90',
  secondaryGrey: '#c4c4c4',
  silver: '#dcdcdf',
  clearWhite: '#ffffff85',
};
