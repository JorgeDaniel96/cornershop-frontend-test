// vendors
import React, { useState, useEffect, createContext } from 'react';

export const CsAppContext = createContext(null);

export const CsAppContextProvider = ({ children, appMessages = [] }) => {
  const [lang, setLang] = useState(localStorage?.currentLang || 'en');
  const messages = appMessages[lang] || {};

  function onSetLang(nextLang) {
    localStorage.currentLang = nextLang;
    setLang(nextLang);
  }

  useEffect(() => {
    if (!lang) {
      const nextLang = localStorage?.currentLang || 'en';
      localStorage.currentLang = nextLang;
      setLang(nextLang);
    }
  }, []);

  return (
    <CsAppContext.Provider
      value={{
        lang,
        messages,
        setLang: onSetLang,
      }}
    >
      {children}
    </CsAppContext.Provider>
  );
};
