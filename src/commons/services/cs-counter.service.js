// commons
import { GET, POST, DELETE, ENDPOINTS } from 'commons';

/**
 * getCounters response
 * Response => []
 **/
export const getCounters = () => {
  const params = {
    endpointKey: ENDPOINTS.GET_COUNTERS.key,
  };

  return GET(params);
};

/**
 * addCounter params
 * @param {object} body { title: "bob" }
 * Response => { id: "asdf", title: "bob", count: 0 }
 **/
export const addCounter = (body) => {
  const params = {
    endpointKey: ENDPOINTS.POST_COUNTER.key,
    body,
  };

  return POST(params);
};

/**
 * incrementCounter params
 * @param {object} body { id: "asdf" }
 * Response => { id: "asdf", title: "bob", count: 1 }
 **/
export const incrementCounter = (body) => {
  const params = {
    endpointKey: ENDPOINTS.INCREMENT_COUNTER.key,
    body,
  };

  return POST(params);
};

/**
 * decrementCounter params
 * @param {object} body { id: "asdf" }
 * Response => { id: "asdf", title: "bob", count: 0 }
 **/
export const decrementCounter = (body) => {
  const params = {
    endpointKey: ENDPOINTS.DECREMENT_COUNTER.key,
    body,
  };

  return POST(params);
};

/**
 * deleteCounter params
 * @param {object} body { id: "qwer" }
 * Response => "qwer" // The id of the deleted counter
 **/
export const deleteCounter = (body) => {
  const params = {
    endpointKey: ENDPOINTS.DELETE_COUNTER.key,
    body,
  };

  return DELETE(params);
};
