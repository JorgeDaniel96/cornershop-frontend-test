import React from 'react';

const DecrementIcon = ({
  size,
  width = '14',
  height = '2',
  className = 'cs-decrement-icon',
  fill = 'var(--dark-black)',
}) => {
  return (
    <svg
      className={className}
      width={size || width}
      height={size || height}
      viewBox='0 0 14 2'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <rect width={size || width} height={size || height} rx='1' fill={fill} />
    </svg>
  );
};

export default DecrementIcon;
