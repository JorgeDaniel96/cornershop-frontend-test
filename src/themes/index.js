// commons
import { device, sizes, AppColors } from 'commons';

// theme obj
export const theme = {
  device,
  sizes,
  colors: { ...AppColors },
};
