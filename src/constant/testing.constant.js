// themes
import { theme } from 'themes';

// translations
import { appMessages } from 'translations';

export const testingProviderOptions = {
  theme,
  messages: appMessages.en,
};
