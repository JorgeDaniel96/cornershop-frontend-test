// vendors
import { IntlProvider } from 'react-intl';
import { ThemeProvider } from 'styled-components';
import React, { Suspense, useContext } from 'react';
import { HelmetProvider } from 'react-helmet-async';
import { BrowserRouter as Router } from 'react-router-dom';

// commons
import {
  CsLoading,
  queryClient,
  CsAppContext,
  ReactQueryProvider,
  CsAppContextProvider,
} from 'commons';

// routes
import { Routes } from 'routes';

// translations
import { appMessages } from 'translations';

// themes
import { theme } from 'themes';

// styles
import { AppContainer, ViewsContainer } from './app.styled';

const App = () => {
  const { lang, messages } = useContext(CsAppContext);
  return (
    <IntlProvider locale={lang} messages={messages} defaultLocale='en'>
      <ViewsContainer>
        <Routes />
      </ViewsContainer>
    </IntlProvider>
  );
};

export default () => (
  <HelmetProvider>
    <ReactQueryProvider client={queryClient}>
      <ThemeProvider theme={theme}>
        <CsAppContextProvider appMessages={appMessages}>
          <AppContainer>
            <Suspense fallback={<CsLoading className='fallback-app-loader' />}>
              <Router>
                <App />
              </Router>
            </Suspense>
          </AppContainer>
        </CsAppContextProvider>
      </ThemeProvider>
    </ReactQueryProvider>
  </HelmetProvider>
);
