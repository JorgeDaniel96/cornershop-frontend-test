// vendors
import React, { lazy } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// constants
import { PATHS } from 'constant';

export function Routes() {
  return (
    <Switch>
      <Route exact path='/'>
        <Redirect to={PATHS.HOME} />
      </Route>
      <Route
        exact
        path={PATHS.HOME}
        component={lazy(() => import('views/home-view'))}
      />
      <Route
        exact
        path={PATHS.DASHBOARD}
        component={lazy(() => import('views/dashboard-view'))}
      />
      <Route
        path=''
        component={lazy(() => import('views/page-not-found-view'))}
      />
    </Switch>
  );
}
