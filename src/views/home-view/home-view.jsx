// vendors
import React from 'react';

// commons
import { CsHelmet, CsButton, useRouter } from 'commons';

// constants
import { PATHS } from 'constant';

// commons
import { useI18n } from 'commons';

// styles
import { Container, Title, Logo, Description } from './home-view.styled';

const BASE_TEST_ID = 'AppHomeView';
const BASE_TRANSLATIONS = 'app.homeView';

export function HomeView() {
  const { goTo } = useRouter();
  const { getMessage } = useI18n();

  function onClickGoToDashboardButton() {
    goTo(PATHS.DASHBOARD);
  }
  return (
    <Container data-testid={`${BASE_TEST_ID}Container`}>
      <CsHelmet titleView={getMessage(BASE_TRANSLATIONS)} />
      <Logo />
      <Title>{getMessage(`${BASE_TRANSLATIONS}.title`)}</Title>
      <Description>
        {getMessage(`${BASE_TRANSLATIONS}.description`)}
      </Description>
      <CsButton onClick={onClickGoToDashboardButton}>
        {getMessage(`${BASE_TRANSLATIONS}.getStartedButton.text`)}
      </CsButton>
    </Container>
  );
}
