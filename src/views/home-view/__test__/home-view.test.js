// vendors
import React from 'react';
import { cleanup, fireEvent } from '@testing-library/react';

// commons
import { CsTestingCustomRender as render } from 'commons';

// HomeView component
import HomeView from '..';

// constants
import { PATHS, testingProviderOptions } from 'constant';
import appMessages from 'translations/en.json';

const BASE_TEST_ID = 'AppHomeView';
const BASE_TRANSLATIONS = 'app.homeView';

const getMessage = (id) => appMessages[`${BASE_TRANSLATIONS}.${id}`];

// mocks
const mockGoTo = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: jest
    .fn()
    .mockReturnValue({ environment: 'dev', service: 'fakeService' }),
  useHistory: jest
    .fn()
    .mockReturnValue({ environment: 'dev', service: 'fakeService' }),
  useLocation: jest
    .fn()
    .mockReturnValue({ environment: 'dev', service: 'fakeService' }),
  useRouteMatch: jest
    .fn()
    .mockReturnValue({ environment: 'dev', service: 'fakeService' }),
}));

jest.mock('commons/utils/hooks/useRouter.js', () => ({
  useRouter: () => ({
    goTo: mockGoTo,
  }),
}));

describe('Home view test suit', () => {
  beforeEach(() => {
    cleanup();
  });

  it('renders HomeView successfully', () => {
    const { getByTestId, getByText } = render(
      <HomeView />,
      testingProviderOptions
    );

    const HomeViewContainer = getByTestId(`${BASE_TEST_ID}Container`);
    const HomeViewLogo = getByText('logo.svg');
    const HomeViewTitle = getByText(getMessage('title'));
    const HomeViewDescription = getByText(getMessage('description'));

    expect(HomeViewContainer).toBeInTheDocument();
    expect(HomeViewLogo).toBeInTheDocument();
    expect(HomeViewTitle).toBeInTheDocument();
    expect(HomeViewDescription).toBeInTheDocument();
  });

  it(`should trigger goTo with "${PATHS.DASHBOARD}" as destination route`, () => {
    const { getByText } = render(<HomeView />, testingProviderOptions);

    const GoToDashboardButton = getByText(getMessage('getStartedButton.text'));

    fireEvent.click(GoToDashboardButton);

    expect(mockGoTo).toHaveBeenCalledWith(PATHS.DASHBOARD);
  });
});
