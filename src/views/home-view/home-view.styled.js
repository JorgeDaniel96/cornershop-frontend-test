// vendors
import styled from 'styled-components';

// images
import { SVG } from 'images';

export const Container = styled.div`
  text-align: center;
  padding-bottom: 108px;
  width: calc(100% - 48px);
`;
Container.displayName = 'HomeViewContainer';

export const Logo = styled(SVG.LogoImg)`
  z-index: 1;
  cursor: pointer;
  margin-top: 115.17px;
  margin-bottom: 103.17px;
  box-sizing: border-box;
`;
Logo.displayName = 'HomeViewLogo';

export const Title = styled.h1`
  margin: 0;
  font-size: 22px;
  font-weight: 600;
  line-height: 30px;
`;
Title.displayName = 'HomeViewTitle';

export const Description = styled.p`
  font-size: 17px;
  line-height: 23px;
  margin: 20px 21px 95px;
`;
Description.displayName = 'HomeViewDescription';
