// vendors
import React from 'react';

// commons
import { CsHelmet, CsButton, useRouter, useI18n } from 'commons';

// constants
import { PATHS } from 'constant';

// styles
import { Container, Title } from './page-not-found-view.styled';

const BASE_TRANSLATIONS = 'app.pageNotFoundView';

export function PageNotFoundView() {
  const { goTo } = useRouter();
  const { getMessage } = useI18n();

  function onClickGoToDashboardButton() {
    goTo(PATHS.DASHBOARD);
  }

  return (
    <Container>
      <CsHelmet titleView={getMessage(`${BASE_TRANSLATIONS}.title`)} />
      <Title>{getMessage(`${BASE_TRANSLATIONS}.title`)}</Title>
      <CsButton onClick={onClickGoToDashboardButton}>
        {getMessage(`${BASE_TRANSLATIONS}.goToDashboardButton.text`)}
      </CsButton>
    </Container>
  );
}
