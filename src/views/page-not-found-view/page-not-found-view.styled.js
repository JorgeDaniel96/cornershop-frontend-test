import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  text-align: center;
`;
Container.displayName = "PageNotFoundContainer";

export const Title = styled.h1`
  width: 100%;
`;
Title.displayName = "PageNotFoundTitle";
