// vendors
import { groupBy } from 'lodash';
import React, { useMemo } from 'react';

// commons
import { CsCloseIcon, CsModal, CsText, useI18n, CsTag } from 'commons';

// styles
import {
  Content,
  Carousel,
  GroupContainer,
} from './dashboard-counter-examples-modal.styled';

// mocks
import { ExamplesMock } from 'mocks';

const BASE_TRANSLATIONS = 'app.dashboardView.CounterExamplesModal';

export function CounterExamplesModal({
  showModal,
  toggleModal,
  onClickExample,
}) {
  const { getMessage } = useI18n();

  const groupedExamples = useMemo(() => {
    // @TODO: replace ExamplesMock data when API is available
    const examplesByGroup = Object.entries(groupBy(ExamplesMock, 'group'));

    const _groupedExamples = examplesByGroup?.map(([group, examples = []]) => (
      <GroupContainer key={group}>
        <CsText weight='500' className='group-text'>
          {getMessage(`${BASE_TRANSLATIONS}.bodyGroup.${group}`)}
        </CsText>
        <Carousel>
          {examples?.map((example) => (
            <CsTag key={example.id} onClick={() => onClickExample(example)}>
              <CsText weight='500'>{example.title}</CsText>
            </CsTag>
          ))}
        </Carousel>
      </GroupContainer>
    ));

    return _groupedExamples;
  }, [ExamplesMock]);

  const [modalHeader, modalBody] = useMemo(() => {
    const MODAL_HEADER = (
      <CsModal.Header>
        <CsCloseIcon onClick={toggleModal} />
        <CsModal.Title type='big'>
          {getMessage(`${BASE_TRANSLATIONS}.titleValue`)}
        </CsModal.Title>
      </CsModal.Header>
    );

    const MODAL_BODY = (
      <CsModal.Body>
        <CsText type='small' className='body-title'>
          {getMessage(`${BASE_TRANSLATIONS}.bodyTitle`)}
        </CsText>
        {groupedExamples}
      </CsModal.Body>
    );

    return [MODAL_HEADER, MODAL_BODY];
  }, [groupedExamples, showModal]);

  return (
    <CsModal showModal={showModal} className='dashboard-counter-examples-modal'>
      <Content>
        {modalHeader}
        {modalBody}
      </Content>
    </CsModal>
  );
}
