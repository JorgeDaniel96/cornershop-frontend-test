// vendors
import styled from 'styled-components';

export const Content = styled.div`
  .cs-modal__header {
    display: flex;
    height: 100%;
    align-items: center;
    .modal-title {
      display: flex;
      .cs-close-icon {
        cursor: pointer;
      }
    }
    .cs-close-icon {
      min-width: 28px;
      cursor: pointer;
      padding: 5px;
      height: 28px;
      margin-right: 16px;
      border-radius: 100%;
      background-color: ${({ theme }) => theme.colors.secondaryGrey};

      * {
        fill: ${({ theme }) => theme.colors.white};
      }
    }
  }

  .cs-modal__body {
    padding: 0;
    margin-top: 28px;
    .body-title {
      padding-left: 16px;
      color: ${({ theme }) => theme.colors.grey};
    }
  }
`;
Content.displayName = 'DashboardViewCounterExampleModalContent';

export const GroupContainer = styled.div`
  margin-top: 30px;
  .group-text {
    margin: 30px 20px 0;
  }
`;
GroupContainer.displayName = 'DashboardViewCounterExampleModalGroupContainer';

export const Carousel = styled.div`
  width: 100%;
  display: flex;
  margin-top: 0;
  overflow: auto;
  margin-top: 9px;
  max-height: 200px;
  padding: 0 16px 0;
  flex-flow: row nowrap;
`;
Carousel.displayName = 'DashboardViewCounterExampleModalCarousel';
