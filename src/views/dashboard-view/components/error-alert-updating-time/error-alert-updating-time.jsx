// vendors
import React from 'react';

// commons
import { useI18n, CsAlertBase } from 'commons';

const BASE_TRANSLATIONS = 'app.dashboardView.errorAlertUpdatingTime';

export function ErrorAlertUpdatingTime({
  counter,
  showAlert = false,
  primaryButtonClick = () => {},
  secondaryButtonClick = () => {},
}) {
  const { getMessage, getFormattedMessage } = useI18n();

  return (
    <CsAlertBase
      showAlert={showAlert}
      primaryButtonClick={primaryButtonClick}
      secondaryButtonClick={secondaryButtonClick}
      title={getFormattedMessage(`${BASE_TRANSLATIONS}.title`, counter)}
      message={getMessage(`${BASE_TRANSLATIONS}.message`)}
      actionButtonContent={getMessage(
        `${BASE_TRANSLATIONS}.actionButtonContent`
      )}
      secondaryActionButtonContent={getMessage(
        `${BASE_TRANSLATIONS}.secondaryActionButtonContent`
      )}
    />
  );
}
