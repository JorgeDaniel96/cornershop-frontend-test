// vendors
import React, { useContext } from 'react';
import { isEmpty } from 'lodash';

// commons
import { CsButton, CsNewIcon, csUseModal, CsVisibility } from 'commons';

// components
import { DeleteCounterButton, ShareCounterButton } from './components';
import { CreateCounterModal } from '../dashboard-create-counter-modal';

// contexts
import { DashboardViewContext } from 'views/dashboard-view/context';

// styles
import { Container, ButtonWrapper } from './dashboard-footer.styled';

const BASE_TEST_ID = 'AppDashboardViewFooter';

export function Footer({
  addButtonConfig,
  shareButtonConfig,
  deleteButtonConfig,
}) {
  const { activeCounter, setActiveCounter } = useContext(DashboardViewContext);
  const [showCreateCounterModal, toggleCreateCounterModal] = csUseModal();

  return (
    <Container data-testid={`${BASE_TEST_ID}`}>
      <CreateCounterModal
        showModal={showCreateCounterModal}
        toggleModal={toggleCreateCounterModal}
      />
      <ButtonWrapper>
        <CsVisibility visible={!isEmpty(activeCounter)}>
          <DeleteCounterButton
            activeCounter={activeCounter}
            buttonConfig={deleteButtonConfig}
            onCounterDeleted={() => setActiveCounter([])}
          />
          <ShareCounterButton
            activeCounter={activeCounter}
            buttonConfig={shareButtonConfig}
          />
        </CsVisibility>
        <CsButton {...addButtonConfig} onClick={toggleCreateCounterModal}>
          <CsNewIcon fill='white' />
        </CsButton>
      </ButtonWrapper>
    </Container>
  );
}
