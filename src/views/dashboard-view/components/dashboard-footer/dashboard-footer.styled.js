// vendors
import styled from 'styled-components';

export const Container = styled.footer`
  left: 0;
  bottom: 0;
  z-index: 5;
  width: 100%;
  display: flex;
  max-width: 100vw;
  position: fixed;
  justify-content: center;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  background-color: ${({ theme }) => theme.colors.white};
`;
Container.displayName = 'DashboardViewFooterContainer';

export const ButtonWrapper = styled.div`
  width: 100%;
  display: flex;
  padding: 15px 16px;
  justify-content: space-between;

  .cs-visibility {
    display: flex;
    .delete-counter-button {
      margin-right: 18px;
    }
  }
  .cs-button {
    &:disabled {
      background-color: ${({ theme }) => theme.colors.silver};
    }
  }
  ${({ theme }) => theme.device.tabletAndDesktop} {
    width: ${({ theme }) => theme.sizes.maxViewsWidth};
  }
`;
ButtonWrapper.displayName = 'DashboardViewFooterButtonWrapper';
