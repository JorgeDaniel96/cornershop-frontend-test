// vendors
import React, { useState, useEffect } from 'react';

// commons
import {
  CsSVG,
  CsText,
  useI18n,
  CsButton,
  CsTooltip,
  CsShareIcon,
  csCopyToClipboard,
} from 'commons';

// styles
import { TooltipContent } from './share-counter-button.styled';

const BASE_TRANSLATIONS = 'app.dashboardViewFooter.shareCounterButton';
const BASE_TRANS_TOOLTIP = `${BASE_TRANSLATIONS}.tooltip`;

export function ShareCounterButton({ buttonConfig, activeCounter }) {
  const { getMessage } = useI18n();
  const [counterTitleCopied, setCounterTitleCopied] = useState(false);

  useEffect(() => {
    return () => {
      setCounterTitleCopied(false);
    };
  }, [activeCounter]);

  const onCopyCounterTitle = () => {
    if (!counterTitleCopied) {
      csCopyToClipboard(activeCounter.title);
      setCounterTitleCopied(true);
    }
  };

  return (
    <div>
      <CsTooltip
        contentPopUp={() => (
          <TooltipContent>
            <TooltipContent.Left>
              <CsText type='big'>
                {getMessage(`${BASE_TRANS_TOOLTIP}.title`)}
              </CsText>
              <CsButton
                kind='raised'
                color='white'
                onClick={onCopyCounterTitle}
                {...buttonConfig}
              >
                <CsText weight='600'>
                  {!counterTitleCopied
                    ? getMessage(`${BASE_TRANS_TOOLTIP}.copyButton`)
                    : getMessage(`${BASE_TRANS_TOOLTIP}.copiedButton`)}
                </CsText>
              </CsButton>
            </TooltipContent.Left>
            <TooltipContent.Right>
              <CsSVG.NotebookImg />
              <CsText weight='600'>
                {activeCounter.count} x {activeCounter.title}
              </CsText>
            </TooltipContent.Right>
          </TooltipContent>
        )}
      >
        {() => (
          <CsButton color='white'>
            <CsShareIcon />
          </CsButton>
        )}
      </CsTooltip>
    </div>
  );
}
