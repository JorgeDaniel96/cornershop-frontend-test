// vendors
import styled from 'styled-components';

export const TooltipCopySection = styled.section`
  display: flex;
  flex-direction: column;

  .cs-button {
    max-width: 90px;
    padding-left: 10px;
    padding-right: 10px;
  }
`;
TooltipCopySection.displayName =
  'DashboardViewFooterShareButtonTooltipContentTooltipCopySection';

export const NotebookImg = styled.div`
  position: relative;
  margin-left: 27px;

  .cs-text {
    left: 32px;
    font-size: 8px;
    min-width: auto;
    max-width: 60px;
    position: absolute;
  }
`;
NotebookImg.displayName =
  'DashboardViewFooterShareButtonTooltipContentNotebookImg';

export const TooltipContent = styled.div`
  display: flex;
`;
TooltipContent.displayName = 'DashboardViewFooterTooltipContent';

TooltipContent.Left = TooltipCopySection;
TooltipContent.Right = NotebookImg;
