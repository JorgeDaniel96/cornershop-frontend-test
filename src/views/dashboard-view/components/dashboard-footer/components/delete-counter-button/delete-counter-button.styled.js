// vendors
import styled from 'styled-components';

// commons
import { CsButton, CsText } from 'commons';

export const DeleteButton = styled(CsButton).attrs(() => ({
  color: 'white',
  id: 'delete-counter-button',
}))`
  margin-right: 18px;
`;
DeleteButton.displayName = 'DashboardViewFooterDeleteCounterButton';

export const AlerConfirmDeleteButton = styled(CsText).attrs(() => ({
  weight: 600,
}))`
  color: ${({ theme }) => theme.colors.destructiveRed};
`;
AlerConfirmDeleteButton.displayName =
  'DashboardViewFooterAlerConfirmDeleteButton';
