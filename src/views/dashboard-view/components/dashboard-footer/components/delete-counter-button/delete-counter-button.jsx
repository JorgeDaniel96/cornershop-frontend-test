// vendors
import React, { useMemo, useEffect } from 'react';

// commons
import {
  csUseMutation,
  CsTrashBinIcon,
  deleteCounterQuery,
  csUseAlert,
  CsAlertBase,
  useI18n,
  AppColors,
} from 'commons';

// styles
import {
  DeleteButton,
  AlerConfirmDeleteButton,
} from './delete-counter-button.styled';

const BASE_TRANSLATIONS = 'app.dashboardViewFooter.deleteCounterButton';
const BASE_TRANS_CONFIRM = `${BASE_TRANSLATIONS}.confirmDeleteCounterAlert`;
const BASE_TRANS_ERROR = `${BASE_TRANSLATIONS}.errorDeleteCounterAlert`;

export function DeleteCounterButton({
  activeCounter,
  buttonConfig,
  onCounterDeleted = () => {},
}) {
  const { getMessage, getFormattedMessage } = useI18n();
  const [showDeleteErrorAlert, toggleDeleteErrorAlert] = csUseAlert(false);
  const [showConfirmDeleteAlert, toggleConfirmDeleteAlert] = csUseAlert(false);
  const { mutate, isError, isSuccess } = csUseMutation(deleteCounterQuery());

  useEffect(() => {
    if (isSuccess) {
      onCounterDeleted();
      toggleDeleteErrorAlert(false);
      toggleConfirmDeleteAlert(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isError) {
      toggleConfirmDeleteAlert(false);
      toggleDeleteErrorAlert(true);
    }
  }, [isError]);

  const renderErrorAlert = useMemo(
    () => (
      <CsAlertBase
        showAlert={showDeleteErrorAlert}
        secondaryButtonClick={() => toggleDeleteErrorAlert(false)}
        primaryButtonClick={() => mutate(activeCounter)}
        title={getFormattedMessage(`${BASE_TRANS_ERROR}.title`, activeCounter)}
        message={getMessage(`${BASE_TRANS_ERROR}.message`)}
        secondaryActionButtonContent={getMessage(
          `${BASE_TRANS_ERROR}.actionButtonContent`
        )}
        actionButtonContent={getMessage(
          `${BASE_TRANS_ERROR}.secondaryActionButtonContent`
        )}
      />
    ),
    [showDeleteErrorAlert]
  );

  const renderDeleteConfirmAlert = useMemo(
    () => (
      <CsAlertBase
        showAlert={showConfirmDeleteAlert}
        primaryButtonClick={toggleConfirmDeleteAlert}
        secondaryButtonClick={() => mutate(activeCounter)}
        title={getFormattedMessage(
          `${BASE_TRANS_CONFIRM}.title`,
          activeCounter
        )}
        message={getMessage(`${BASE_TRANS_CONFIRM}.message`)}
        secondaryActionButtonContent={
          <AlerConfirmDeleteButton>
            {getMessage(`${BASE_TRANS_CONFIRM}.actionButtonContent`)}
          </AlerConfirmDeleteButton>
        }
        actionButtonContent={getMessage(
          `${BASE_TRANS_CONFIRM}.secondaryActionButtonContent`
        )}
      />
    ),
    [showConfirmDeleteAlert]
  );

  return (
    <DeleteButton onClick={toggleConfirmDeleteAlert} {...buttonConfig}>
      {renderErrorAlert}
      {renderDeleteConfirmAlert}
      <CsTrashBinIcon fill={AppColors.destructiveRed} />
    </DeleteButton>
  );
}
