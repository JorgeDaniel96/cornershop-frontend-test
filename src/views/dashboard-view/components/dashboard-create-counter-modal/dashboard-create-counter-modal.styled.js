// vendors
import styled from 'styled-components';

export const Content = styled.div`
  .cs-loading-container {
    top: 71px;
  }
  .cs-modal__header {
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    .modal-title {
      display: flex;
      .cs-close-icon {
        cursor: pointer;
      }
    }
    .cs-close-icon {
      min-width: 28px;
      padding: 5px;
      height: 28px;
      cursor: pointer;
      margin-right: 16px;
      border-radius: 100%;
      background-color: ${({ theme }) => theme.colors.secondaryGrey};

      * {
        fill: ${({ theme }) => theme.colors.white};
      }
    }
  }

  .cs-modal__body {
    .cs-input {
      margin: 9px 0;
    }
    .see-examples {
      color: ${({ theme }) => theme.colors.grey};
      a {
        cursor: pointer;
        text-decoration: underline;
      }
    }
  }
`;
Content.displayName = 'DashboardViewCreateCounterModalContent';
