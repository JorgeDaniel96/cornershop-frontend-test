// vendors
import React, { useMemo, useEffect } from 'react';

// commons
import {
  CsText,
  useI18n,
  CsInput,
  CsModal,
  CsButton,
  CsLoading,
  csUseAlert,
  csUseModal,
  csUseInput,
  CsCloseIcon,
  CsAlertBase,
  csUseMutation,
  addCounterQuery,
} from 'commons';

// styles
import { Content } from './dashboard-create-counter-modal.styled';
import { CounterExamplesModal } from '../dashboard-counter-examples-modal';

const BASE_TRANSLATIONS = 'app.dashboardView.CreateCounterModal';

export function CreateCounterModal({ showModal, toggleModal }) {
  const { getMessage } = useI18n();
  const [showAlert, toggleAlert] = csUseAlert();
  const [showExamplesModal, toggleExamplesModal] = csUseModal();
  const { isSuccess, isError, isLoading, mutate } = csUseMutation(
    addCounterQuery
  );

  const { inpuValue, setInpuValue, isInputEmpty } = csUseInput();

  const onAddCounter = () => {
    const body = { title: inpuValue };
    mutate(body);
  };

  useEffect(() => {
    if (isSuccess) {
      toggleModal();
      setInpuValue('');
    }
    (isSuccess || isError) && toggleAlert();
  }, [isSuccess, isError]);

  const [modalHeader, modalBody] = useMemo(() => {
    const MODAL_HEADER = (
      <CsModal.Header>
        <CsModal.Title type='big'>
          <CsCloseIcon onClick={toggleModal} />
          {getMessage(`${BASE_TRANSLATIONS}.title.createCounter`)}
        </CsModal.Title>
        <CsButton onClick={onAddCounter} disabled={isInputEmpty}>
          {getMessage(`${BASE_TRANSLATIONS}.title.saveButton`)}
        </CsButton>
      </CsModal.Header>
    );

    const MODAL_BODY = (
      <CsModal.Body>
        <CsText weight='600'>
          {getMessage(`${BASE_TRANSLATIONS}.body.counterName`)}
        </CsText>
        <CsInput
          value={inpuValue}
          onChange={setInpuValue}
          onEnterPressed={onAddCounter}
          placeholder={getMessage(`${BASE_TRANSLATIONS}.body.placeholderInput`)}
        />
        <CsText className='see-examples' type='small'>
          {getMessage(`${BASE_TRANSLATIONS}.body.seeExamplesText`)}
          <CsText as='a' type='small' onClick={toggleExamplesModal}>
            {getMessage(`${BASE_TRANSLATIONS}.body.seeExamplesLink`)}
          </CsText>
        </CsText>
      </CsModal.Body>
    );

    return [MODAL_HEADER, MODAL_BODY];
  }, [showModal, inpuValue]);

  const [alertTitle, alertMessage, alertActionButtonContent] = useMemo(() => {
    const transId = isSuccess ? 'successAlert' : isError && 'errorAlert';
    const title = getMessage(`${BASE_TRANSLATIONS}.${transId}.title`);
    const message = getMessage(`${BASE_TRANSLATIONS}.${transId}.message`);
    const buttonContent = getMessage(`${BASE_TRANSLATIONS}.alert.actionButton`);

    return [title, message, buttonContent];
  }, [isSuccess, isError]);

  const onClickExample = (example) => {
    setInpuValue(example.title);
    toggleExamplesModal(false);
  };

  return (
    <>
      <CounterExamplesModal
        showModal={showExamplesModal}
        onClickExample={onClickExample}
        toggleModal={toggleExamplesModal}
      />
      <CsAlertBase
        showAlert={showAlert}
        title={alertTitle}
        message={alertMessage}
        primaryButtonClick={toggleAlert}
        actionButtonContent={alertActionButtonContent}
      />
      <CsModal showModal={showModal}>
        <Content>
          {isLoading ? <CsLoading /> : <></>}
          {modalHeader}
          {modalBody}
        </Content>
      </CsModal>
    </>
  );
}
