// vendors
import styled, { css } from 'styled-components';

const CounterListDisabledStyles = css`
  height: 100%;
  opacity: 0.3;
  background-color: ${({ theme }) => theme.colors.clearWhite};

  .cs-decrement-icon *,
  .cs-increment-icon * {
    fill: ${({ theme }) => theme.colors.silver};
  }
`;

const activeCounterStyles = css`
  .counters-count-times .counters-total-times {
    margin-left: 0;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.appTint};
  }
`;
export const Container = styled.div`
  .counters-count-times {
    width: 100%;
    display: flex;
    text-align: left;
    padding: 0 0 0 16px;
    .counters-total-times,
    .counters-total-times-refreshing {
      margin: 0 8px;
      display: flex;
      align-items: center;
      color: ${({ theme }) => theme.colors.grey};

      svg {
        cursor: pointer;
        margin-left: 8.4px;
      }
    }
    .counters-total-times-refreshing {
      color: ${({ theme }) => theme.colors.appTint};
    }
  }

  svg * {
    fill: ${({ theme, refreshing = false }) =>
      theme.colors[refreshing ? 'appTint' : 'black']};
  }

  ${({ isActiveCounter = false }) => isActiveCounter && activeCounterStyles};
  ${({ disabled = false }) => disabled && CounterListDisabledStyles};
`;
Container.displayName = 'DashboardViewCounterListContainer';

export const EmptyContainer = styled.div`
  top: 0;
  left: 0;
  height: 100%;
  display: flex;
  padding: 0 40px;
  position: absolute;
  flex-direction: column;
  justify-content: center;

  ${({ theme }) => theme.device.tabletAndDesktop} {
    margin: 0 auto;
    max-width: 450px;
    position: initial;
    justify-content: flex-start;
  }
`;
EmptyContainer.displayName = 'DashboardViewEmptyCountersContainer';

export const EmptyTitle = styled.h4`
  font-size: 22px;
  margin-bottom: 0;
  font-weight: 600;
`;
EmptyTitle.displayName = 'DashboardViewEmptyCountersTitle';

export const EmptyBody = styled.p`
  font-size: 17px;
`;
EmptyBody.displayName = 'DashboardViewEmptyCounterBody';
