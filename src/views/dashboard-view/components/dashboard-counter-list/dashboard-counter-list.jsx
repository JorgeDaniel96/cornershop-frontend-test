// vendors
import { isEmpty } from 'lodash';
import React, { useMemo, useContext } from 'react';

// commons
import { CsSVG, CsText, useI18n, CsCounter, csGetTotalTimes } from 'commons';

// context
import { DashboardViewContext } from '../../context';

// styles
import {
  Container,
  EmptyBody,
  EmptyTitle,
  EmptyContainer,
} from './dashboard-counter-list.styled';

const BASE_TRANSLATIONS = 'app.dashboardView.counterList';

export const CounterList = ({
  data = [],
  disabled = false,
  refreshing = false,
  onIncrease = () => {},
  onDecrease = () => {},
  onClickRefreshIcon = () => {},
}) => {
  const { getMessage } = useI18n();
  const { activeCounter, setActiveCounter } = useContext(DashboardViewContext);

  const totalTimes = useMemo(() => csGetTotalTimes(data), [data]);

  const isActiveCounter = !isEmpty(activeCounter);

  if (isEmpty(data)) {
    return (
      <EmptyContainer>
        <EmptyTitle>
          {getMessage(`${BASE_TRANSLATIONS}.emptyMessageTitle`)}
        </EmptyTitle>
        <EmptyBody>
          {getMessage(`${BASE_TRANSLATIONS}.emptyMessageDescription`)}
        </EmptyBody>
      </EmptyContainer>
    );
  }

  return (
    <Container
      isActiveCounter={isActiveCounter}
      refreshing={refreshing}
      disabled={disabled}
    >
      <CsText as='div' className='counters-count-times'>
        {!isActiveCounter && (
          <CsText weight='600'>
            {data?.length}
            {getMessage(`${BASE_TRANSLATIONS}.items`)}
          </CsText>
        )}
        <CsText className='counters-total-times'>
          {isActiveCounter ? 1 : totalTimes}
          {getMessage(
            `${BASE_TRANSLATIONS}.${isActiveCounter ? 'selected' : 'times'}`
          )}
          <CsSVG.CsRefreshIcon onClick={onClickRefreshIcon} />
        </CsText>
        {refreshing && (
          <CsText className='counters-total-times-refreshing'>
            {getMessage(`${BASE_TRANSLATIONS}.refreshing`)}
          </CsText>
        )}
      </CsText>
      {data?.map((counter) => (
        <CsCounter
          key={counter.id}
          counter={counter}
          onIncrease={onIncrease}
          onDecrease={onDecrease}
          onClick={() => setActiveCounter(counter)}
          isActive={activeCounter?.id === counter?.id}
        />
      ))}
    </Container>
  );
};
