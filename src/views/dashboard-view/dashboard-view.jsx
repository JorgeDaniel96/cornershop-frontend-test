// vendors
import { isEmpty } from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';

// commons;
import {
  CsText,
  useI18n,
  CsButton,
  CsHelmet,
  CsLoading,
  csUseAlert,
  CsSearcher,
  csUseQuery,
  csUseSercher,
  getCountersQuery,
  csFilterDataByKey,
} from 'commons';

// components
import { Footer, CounterList, ErrorAlertUpdatingTime } from './components';

// custom hooks
import { useCounter } from './hooks';

// styles
import { Container, ErrorFetchingCounters } from './dashboard-view.styled';

const BASE_TEST_ID = 'AppDashboardView';
const BASE_TRANSLATIONS = 'app.dashboardView';

export function DashboardView() {
  const { getMessage } = useI18n();
  const [showAlert, toggleAlert] = csUseAlert();
  const { incrementCounter, decrementCounter, counterInError } = useCounter({
    showAlert,
    toggleAlert,
  });
  const [isSearchFocused, setIsSearchFocused] = useState(false);

  const {
    searchInputValue,
    setSearchInputValue,
    onSearchInputChange,
  } = csUseSercher('');

  const {
    isError,
    isSuccess,
    isFetching,
    data: counterList,
    refetch: getCounters,
  } = csUseQuery(getCountersQuery);

  useEffect(() => {
    if (!counterList) {
      getCounters();
    }
  }, []);

  // returns counterList filtered by searchInputValue
  const countersListToShow = useMemo(
    () => csFilterDataByKey(searchInputValue, counterList),
    [searchInputValue, counterList]
  );

  const renderCounterList = (
    <CounterList
      refreshing={isFetching}
      data={countersListToShow}
      onIncrease={incrementCounter}
      onDecrease={decrementCounter}
      onClickRefreshIcon={getCounters}
      disabled={isSearchFocused && isEmpty(searchInputValue)}
    />
  );

  const renderErrorFetchingCountersMessage = (
    <ErrorFetchingCounters>
      <CsText as='h2' type='big'>
        {getMessage(`${BASE_TRANSLATIONS}.errorFetchingCounters.title`)}
      </CsText>
      <CsText type='small'>
        {getMessage(`${BASE_TRANSLATIONS}.errorFetchingCounters.body`)}
      </CsText>
      <CsButton onClick={getCounters} color='danger'>
        {getMessage(`${BASE_TRANSLATIONS}.errorFetchingCounters.actionButton`)}
      </CsButton>
    </ErrorFetchingCounters>
  );

  const renderSearcher = (
    <CsSearcher
      onFocusSercher={setIsSearchFocused}
      disabled={isEmpty(counterList)}
      inputConfig={{
        value: searchInputValue,
        onChange: onSearchInputChange,
        placeholder: getMessage(`${BASE_TRANSLATIONS}.searchInput.placeholder`),
      }}
      buttonConfig={{
        onClick: () => setSearchInputValue(''),
        content: getMessage(`${BASE_TRANSLATIONS}.searchInput.cancelButton`),
      }}
    />
  );

  const renderErrorAlertUpdatingTime = (
    <ErrorAlertUpdatingTime
      showAlert={showAlert}
      counter={counterInError}
      secondaryButtonClick={() => toggleAlert(false)}
      primaryButtonClick={() =>
        counterInError.isIncrement
          ? incrementCounter(counterInError)
          : decrementCounter(counterInError)
      }
    />
  );

  const renderEmptyCountersMessage = (
    <CsText className='search-counter-no-results'>
      {getMessage(`${BASE_TRANSLATIONS}.searchCounter.noResult`)}
    </CsText>
  );

  const renderFooter = (
    <Footer
      addButtonConfig={{
        disabled: isSearchFocused && isEmpty(searchInputValue),
      }}
    />
  );

  return (
    <Container data-testid={`${BASE_TEST_ID}Container`}>
      <CsHelmet titleView={getMessage(BASE_TRANSLATIONS)} />
      {isFetching && isEmpty(countersListToShow) && <CsLoading />}
      {renderErrorAlertUpdatingTime}
      {renderSearcher}
      {isError && renderErrorFetchingCountersMessage}
      {isSuccess && renderCounterList}
      {isEmpty(countersListToShow) && renderEmptyCountersMessage}
      {renderFooter}
    </Container>
  );
}
