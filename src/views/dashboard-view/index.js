import { DashboardView } from './dashboard-view';
import { DashboardViewContextProvider } from './context';

const DashboardWithProviders = () => (
  <DashboardViewContextProvider>
    <DashboardView />
  </DashboardViewContextProvider>
);

export default DashboardWithProviders;
