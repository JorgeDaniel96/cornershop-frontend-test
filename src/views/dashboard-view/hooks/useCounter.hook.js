// commons
import {
  csUseMutation,
  incrementCounterQuery,
  decrementCounterQuery,
} from 'commons';

let counterInError = {};

export function useCounter({ showAlert = false, toggleAlert }) {
  const onUpdateTimeError = (counter) => {
    counterInError = counter;
    !showAlert && toggleAlert();
  };

  // mutation to increment counter
  const { mutate: incrementCounter } = csUseMutation(
    incrementCounterQuery({
      onUpdateTimeError,
      onUpdateTimeSuccess: () => toggleAlert(false),
    })
  );

  // mutation to decrement counter
  const { mutate: decrementCounter } = csUseMutation(
    decrementCounterQuery({
      onUpdateTimeError,
      onUpdateTimeSuccess: () => toggleAlert(false),
    })
  );

  return { incrementCounter, decrementCounter, counterInError };
}
