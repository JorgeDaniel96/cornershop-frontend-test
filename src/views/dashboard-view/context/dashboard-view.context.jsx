// vendors
import { isEmpty } from 'lodash';
import React, { useState, createContext } from 'react';

export const DashboardViewContext = createContext(null);

export const DashboardViewContextProvider = ({ children }) => {
  const [activeCounter, setActiveCounter] = useState({});

  const onSetActiveCounter = (selectedCounter) => {
    if (isEmpty(activeCounter) || selectedCounter?.id !== activeCounter?.id) {
      return setActiveCounter(selectedCounter);
    }
    return setActiveCounter({});
  };

  return (
    <DashboardViewContext.Provider
      value={{
        activeCounter,
        setActiveCounter: onSetActiveCounter,
      }}
    >
      {children}
    </DashboardViewContext.Provider>
  );
};
