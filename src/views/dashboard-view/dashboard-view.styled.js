// vendors
import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 16px;
  text-align: center;
  padding-bottom: 80px;
  width: calc(100% - 32px);

  .search-counter-no-results {
    display: flex;
    font-size: 22px;
    align-items: center;
    justify-content: center;
    height: calc(100% - 55px);
    color: ${({ theme }) => theme.colors.grey};
  }
  ${({ theme }) => theme.device.tabletAndDesktop} {
    max-width: ${({ theme }) => theme.sizes.maxViewsWidth};
  }
`;
Container.displayName = 'DashboardViewContainer';

export const ErrorFetchingCounters = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  padding: 0 40px;
  position: absolute;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.clearWhite};
  height: calc(
    100% - ${({ theme }) => theme.sizes.dashboardFooterButtonHeight}
  );

  .cs-button {
    margin-top: 20px;
    color: ${({ theme }) => theme.colors.appTint};
  }
`;
ErrorFetchingCounters.displayName = 'DashboardViewErrorFetchingCounters';
