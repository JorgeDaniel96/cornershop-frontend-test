// vendors
import styled from 'styled-components';

export const AppContainer = styled.div`
  z-index: 1;
  display: flex;
  max-width: 100vw;
  min-height: 100vh;
  justify-content: center;

  #fallback-app-loader {
    height: 100%;
  }
`;
AppContainer.displayName = 'AppContainer';

export const ViewsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
ViewsContainer.displayName = 'ViewsContainer';
